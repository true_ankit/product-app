class CreateProducts < ActiveRecord::Migration[7.0]
  def change
    create_table :products do |t|
      t.string :product_name
      t.string :category_name
      t.text :description
      t.string :created_by
      t.string :status

      t.timestamps
    end
  end
end
