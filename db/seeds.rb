# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Product.create(
  "product_name": 'Cheese',
  "category_name": 'dairy',
  "description": 'Cheese has been used by humans.',
  "created_by": 'new village farmer',
  "status": 'In stock'
)

Product.create(
  "product_name": 'Butter',
  "category_name": 'dairy',
  "description": 'Butter, mostly milk fat, produced by churning cream.',
  "created_by": 'new village farmer',
  "status": 'In stock'
)

Product.create(
  "product_name": 'Wood Chair',
  "category_name": 'furniture',
  "description": 'Comfortable and affordable',
  "created_by": 'Lumbini Furniture',
  "status": 'In stock'
)

Product.create(
  "product_name": 'Washing Machine',
  "category_name": 'electronic',
  "description": 'Protect your fabric',
  "created_by": 'Roshan Electronic',
  "status": 'Limited Available'
)

Product.create(
  "product_name": 'Yogurt',
  "category_name": 'dairy',
  "description": 'Yogurt, milk fermented by thermophilic bacteria.',
  "created_by": 'kathmandu dairy product',
  "status": 'In stock'
)

Product.create(
  "product_name": 'Basundi',
  "category_name": 'dairy',
  "description": ' It is a sweetened condensed milk made by boiling milk',
  "created_by": 'kathmandu dairy product',
  "status": 'In stock'
)

Product.create(
  "product_name": 'Table',
  "category_name": 'furniture',
  "description": 'Comfortable with high quality wood quality',
  "created_by": 'kathmandu Furniture',
  "status": 'In stock'
)

Product.create(
  "product_name": 'TV',
  "category_name": 'electronic',
  "description": 'High Quality Display',
  "created_by": 'Sandesh Electronics',
  "status": 'In stock'
)

Product.create(
  "product_name": 'JBL Speaker',
  "category_name": 'electronic',
  "description": 'High Quality Sound with long battery life',
  "created_by": 'Sandesh Electronic',
  "status": 'In stock'
)

Product.create(
  "product_name": 'IPhone',
  "category_name": 'electronic',
  "description": 'High Quality smart device',
  "created_by": 'iPhone Official Store',
  "status": 'In stock'
)

Product.create(
  "product_name": 'Smart Watch',
  "category_name": 'electronic',
  "description": 'High Quality Water proof watch',
  "created_by": 'Roshan Electronic',
  "status": 'In stock'
)

Product.create(
  "product_name": 'milk',
  "category_name": 'dairy',
  "created_by": 'natural dairy product',
  "description": 'milk is great for your health.',
  "status": 'In stock'
)

Product.create(
  "product_name": 'bed2',
  "category_name": 'furniture',
  "created_by": 'new home furniture',
  "description": 'description',
  "status": 'Limited Available'
)
