class Product < ApplicationRecord
  validates :product_name, presence: true

  enum status: { in_stock: 'In stock', out_off_stock: 'Out off stock', limited_available: 'Limited Available' }
  enum category_name: { electronic: 'electronic', furniture: 'furniture', dairy: 'dairy' }
end
